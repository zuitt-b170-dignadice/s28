/* 
    What is an API (REST)
        - Application programming Interface
        the part of a server responsible for receiving requests and sending responses.

        - just as a bank teller stands between a customer and the bank's vault, an API acts as the middleman between the front and back ends of an appliccation

         application >< API >< Program

        Hides the server beneath an interface layer
            Hidden compexity makes apps easier to use
            Sets the rules of interaction between front and back ends of an application, improving security.
        
        REST = an architecutral style for providing standards for communication between computer systems.
        The need to separate user interface concerns of th eclient from data storage concerns of the server.

        Statelessness - server does not need to know the client state and vice versa.

        every client request has all the information it needs to be processed by the API.
        
        Standardized Communication
            Enables a decoupled server to udnerstand, process, and respond to client requests without knowing client state

        Implemented via resources represented as UNIFORM RESOURCE IDENTIFIER (URI) endpoints and http methods.

        Resources are plural by convention.

        api/photos
        api/statuses 

        (always plural)

        Anatomy of a Client Request 
            Operation to be preformed dictated by http methods
            GET
            POST 
            PUT
            DELETE 
        HTTP methods and corresponding CRUD operation

        PATH to the resource to be operated on
        URI endpoint

        HEADER - containing addtional request information

        In the URI 
        api/jokes/programming/random
        resource is JOKES
        /programming - denotes the resource category/
        /random - determines the specfic joke that will be set via response.

        Hitting enter on your browser sends a get request to the API endpoint.

        Given a URI endpoint and a method, the API was able to respond

        POSTMAN API CLient ++++++++++++++++++++++

        improve the experience of sending req, instpecting responses, and debugging. 

*/

// JS Synchronous vs JS Synchronous
// Synchronous Programming - only one statement / line of codes is being processed at a time; being used by javascript by default.

// the errr checking proves the synchronous programming of javascript since the after detecting the error, the next lines of codes will not be executed even if they have no errors.           

console.log('Hello World');
console.log('Hello Again');
console.log('Goodbye ');

// when certain statements take a lot of time to process, this slows down the running/execuring of codes 

// an example of this is when loops are used on a large amount of information or when fetching data form database

// when an aciton takes some time to be executed, this results in "code blocking"
    // code blocking - delaying of a more efficient code compared to ones currently executed.

console.log('Hello World');
// we might not notice it due to improved processessing power of out devices, but the process of fetching/using large amounts of information in our loops takes too much time compatred to logging " hello again"

// another example is when you try to access a website and it takes a while to load (white webpage is being displayed befre the landing page is loaded)

/* 
for (let i = 0; i <= 1500; i++){
    console.log(i)
};
*/


console.log("Hellow Again")


// FETCH Function API - allows us to asynchronously fetch/request for a resource (data)
// a "promise" is an object that represents eventual completion (or failur) of an asynchronous function and its resulting 
/*
Syntax : 
    fetch('url')
*/
console.log(fetch("https://jsonplaceholder.typicode.com/posts"))

// retrieves all posts following the REST APO method (read/GET)
// by using the .then method, we can now check the sstatus of the promise

fetch("https://jsonplaceholder.typicode.com/posts")
// since fetch method returns a "promise", the "then" method will catch the promise and make it the "response" object
// also, the "then" method captures the "response" object and returns another promise which will eventually be rejected/resolved.
.then(response => console.log(response.status))

console.log('hello again');
/* 
    Syntax : 
        fetch(url).then(parameter = {statement})
        .then(parameter => statement)
*/
fetch("https://jsonplaceholder.typicode.com/posts")
// the use of "json()" is to convert the response object into json format to be used by the application.

.then((response) => response.json())
// since we cannot directly print the json format of th response in the second .then method, we need another .then method to catch the promiose and print the "response.json()" which is being represented by the json parameter.

// using then methods multiple times would create promise chains
.then((json) => console.log(json))

// ASYNC-AWAIT 
// keywords is another approach that can be used to achieve js asynchronous
// used in functions to indicate which portions of code should be waited .
// the codes outside the functions will be executed under JS asynchronous.

async function fetchData() {
    let result = await fetch("https://jsonplaceholder.typicode.com/posts")
    // returns a promise
    console.log(result);
    // returns the type of data the "result" variable has
    console.log(typeof result);
    // we cannot acces the body of the result
    console.log(result.body)
    // converts the data from the result variable and stores it in json variable
    let json = await result.json();
    console.log(json);
}
fetchData();
console.log("Hello Again");
console.log("Hello Again");

/* 
    Miniactivity
    using the then method, retrieve the first object in the json placeholder url.
        the response should be converted first into json format before being displayed in the console
*/

async function fetchData() {
    let result = await fetch("https://jsonplaceholder.typicode.com/posts")
    let json = await result.json();
    console.log(json[0]);
}
// retrieves a specific oboject using the id (landingUrl.com/posts/:id)

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((json) => console.log(json[0]))        



fetch("https://jsonplaceholder.typicode.com/posts",{
    method : "POST",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        userId : 1,
        title: "New Post",
        body: "Hello World"
    })
})
.then((response) => response.json())
.then((json) => console.log(json));


/* 
    Create another fetch request (the url should contain as the ID endpoint) with PUT method
        just only specify the title
            title : corrected post
*/

// create / ipdate a resource 
/* 
    SYNTAX 
        fetch ("url", {options}, details of the request body)
        .then(response => {})
        .then(json => )
*/


/* 
    PUT - replaces the whole object
    patch - updates the
*/

fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method : "PUT",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        title: "Corrected post",
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

/* 
    Create another fetch request (the url should contain 1 as the id enpoint) with PATCH method
        userID : 1 
        title : Updated Post 

*/

fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method : "PATCH",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        title: "Updated Post",
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// deleting a resoure
fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method : "DELETE"
})



/*
    Filtering Posts --- 
    the data/result coming from the fetchmethod can be filtered by sending a key value pair along with its url

    the information is sent via the URL can be done by adding the question mark symbol (?)

    SYNTAX 
        -url?parameterName=value" > for single parameter 
        -url?parameterName=value&id=3" > multiple parameter 

*/

// single parameter 
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then((response) => response.json())
.then((json) => console.log(json));

// multiple parameter
fetch("https://jsonplaceholder.typicode.com/posts?userId=1&id=3")
.then((response) => response.json())
.then((json) => console.log(json));

/* 
    Retrieve the nested comments array in the first entry using get method
*/

// Retrieving nested/realted comments to posts
// GET methos url/posts/:id/comments

fetch("https://jsonplaceholder.typicode.com/posts/?userid=1/comments")
.then((response)=>response.json())
.then((json)=>console.log(json));

