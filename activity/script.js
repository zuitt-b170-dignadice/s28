async function fetchData() {
    let result = await fetch("https://jsonplaceholder.typicode.com/todos")
    //  Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
    console.log(result);
    // converts the data from the result variable and stores it in json variable
    let json = await result.json();
    console.log(json);
}
fetchData();


//Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
    let list = json.map((todo => {
        return todo.title;
    }))

    console.log(list);


})

//Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
async function fetchData3() {
    let result = await fetch("https://jsonplaceholder.typicode.com/todos")
    let json = await result.json();
    console.log(json[0]);
    console.log(typeof json)
    //Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
}
fetchData3();

/*
 fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(`the `) 
*/

//Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos",{
    method : "POST",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        userId : 1,
        id : "123",
        title: "Hellow Jherson",
        completed : false
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/2",{
    method : "PUT",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        userId : 1,
        id : "123",
        title: "Hellow Jherson",
        completed : false
    })
})
.then((response) => response.json())
.then((json) => console.log(json));


/* 
Update a to do list itecm by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID
 */

fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method : "PATCH",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        userId : 1,
        id : "123",
        title: "Pla",
        dateCompleted : "Feb 2022",
        completed : false
        
    })
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/2",{
    method : "PUT",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        userId : 1,
        completed : true

    })
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/3",{
    method : "DELETE"
})


